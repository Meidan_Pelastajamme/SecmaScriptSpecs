# SecmaScript-1

## About the specification
This specification is made to be a saner alternative to one of the most important specifications in programming languages with it's [major implementation (JS)](https://developer.mozilla.org/fr/docs/Web/JavaScript)

## How to contribute
You can contribute like you would do with any other code repo at the [official git repo](https://codeberg.org/Meidan_Pelastajamme/SecmaScriptSpecs/).

## Conformance
A conforming implementation, **at least** implements the specification, but may add new things, while still keeping 100% compatibility to this specification.

## Introductory explanation
Little facts that will help you understand the specification :
- In type definitions we use `->` and `*` instead of `:` and `,`
    - `*` example :
        actual tuple : `(1, 2)`
        tuple's type : `(Int * Int)`
    - `->` example :
        actual function definition expression : `(a: Int, b: Int): Int`
        the function's type definition : `(Int * Int) -> Int`

## Conventions

### Variables
Variables should be `snake_case`.

### Constants
Constants should be `FULLY_CAPITALISED`.

### Functions
Functions should be `snake_case`.

### Structures & Types
Structures should be in `PascalCase`

## Comments
```
// Single line comment
```

```
/* 
Multi 
line 
comment
*/
```

## Types
Types' first letter must be capitalised.
The specification supports: Int, Float, Bool, Char, String, Struct, Enum, Tuple, List
All implementions must implement a shipped `type_of` function out of the box that give the type of its sole argument without any complicated mess.  

### Int
Type name : `Int`
The int type represents whole numerical values (ex: 7 not 7.7).

### Float
Type name : `Float`
The float type represents floating point numerical values (ex: 7.0 and 7.7).

### Char
Type name : `Char`
The char type represents a UTF-8 character and should be single quoted (ex: 'a' not "a").

### Str
Type name : `Str`
The string type represents a UTF-8 list of characters.

### Any
Type name: `Any`
The any type accept all types except the unit type

### T 
T is a generic type that can truly represent any type

## Operations
Name              | Symbol         | Position | Example                               | Type restriction
------------------|----------------|----------|---------------------------------------|-----------------------------
Addition          | `+`            | Infix    | `1 + 1` is `2`                        | `Int`, `Float`
Subtraction       | `-`            | Infix    | `1 - 1` is `0`                        | `Int`, `Float`
Multiplication    | `*`            | Infix    | `3 - 2` is `6`                        | `Int`, `Float`
Division          | `/`            | Infix    | `6 - 2` is `3`                        | `Int`, `Float`
Modulo            | `%`            | Infix    | `3 / 2` is `1`                        | `Int`, `Float`
Exponentiation    | `^`            | Infix    | `3 ^ 2` is `9`                        | `Int`, `Float`
Equal             | `==`           | Infix    | `3 == 3` is `true`                    | `Any`
Not equal         | `!=`           | Infix    | `3 != 3` is `false`                   | `Any`
Inferior          | `<`            | Infix    | `3 < 2` is `false`                    | `Int`, `Float`
Inferior or equal | `<=`           | Infix    | `3 <= 3` is `true`                    | `Int`, `Float`
Superior          | `>`            | Infix    | `3 > 2` is `true`                     | `Int`, `Float`
Superior or equal | `>=`           | Infix    | `3 >= 3` is `true`                    | `Int`, `Float`
Concatenation     | `<>`           | Infix    | `'a' <> 'b'` is `"ab"`                | `Char` `Char`, `Str` `Str`
Concatenation     | `::`           | Infix    | `[1] : 2` is `[1, 2]`                 | `[Any] | []` `Any`, `[Any] | []`
Intersection      | `inter`        | Infix    | `[1, 2] inter [3, 1]` is `[1]`        | `[Any] | []` `[Any] | []`, `[Any] | []`
Union             | `union`        | Infix    | `[1, 2] union [3, 1]` is `[1, 2, 3]`  | `[Any] | []` `[Any] | []`, `[Any] | []`
Logical not       | `!`            | Prefix   | `!true` is false and `!!true` is true | `Bool`, `Bool`
Logical and       | `and` or `&&`  | Infix    | `true and false` is false             | `Bool` `Bool`, `Bool`
Logical and       | `or` or `\|\|` | Infix    | `true or false` is true               | `Bool` `Bool`, `Bool`
Composition       | `.`            | Infix    | `f . g(x)` is `f(g(x))`               | `(T -> T)` `(T -> T)`


### Function
#### Definition

Keyword : `func`

Example :
```
func add(x: Int, y: Int): Int {
    return x + y
}
```
#### Call
Example : 
```
add(1, 2)
```
#### Type
`<Parameters' types> -> <Returned type>`
No parameters that returns a generic type T : `() -> T`
Takes two ints and return one : `Int * Int -> Int`

### Variable
Keyword : `let`

Example : 
```
let x = 2
```

Mutation : 
```
x = 2
```
mutation can be coupled with an operator (before the equal sign) :
```
x <operator>= y
```

### Constant
Keyword : `const`

Example : 
```
const x = 2
```

### Control flows

#### If satement
Keyword : `if` `else`

```
if (x == true) {
    print(x)
}
```

#### Infinite loop
Keyword : `loop` `break` `continue`

```
loop {
    print("This is printed indefinitly")
}
```

```
loop {
    print("Printed only one time")
    break
}
```

#### While loop 
Keyword : `while` `break` `continue`

```
let i = 0
while (i < 2) {
    print(i)
    i += 1
}
```

#### For loop 
Keyword : `for`

```
let xs = [1, 2, 3]
for (let i = 0; i < 2; i += 1) {
    print(xs[i])
}
```
Will print `123`


#### For in loop 
Keyword : `for`, `in`

```
let xs = [1, 2, 3]
for x in xs {
    print(x)
}
```
Will print `123`




